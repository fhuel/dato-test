module.exports = {
  siteMetadata: {
    title: 'cariaggi',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sass',
    // {
    //   resolve: 'gatsby-source-contentful',
    //   options: {
    //     spaceId: 'h5oiutp2ggs3',
    //     accessToken:
    //       'b5cc723a3eb1ee36d0a6c9df73d45a03bd46ef16159f3e10b8a010309a089e36',
    //   },
    // },
    {
      resolve: 'gatsby-source-datocms',
      options: {
        apiToken: '8c0749821c84597771fed30d78073d',
      },
    },
  ],
}
