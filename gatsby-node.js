/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require('path')
exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators
  return new Promise((resolve, reject) => {
    const pageTemplate = path.resolve('src/templates/page.js')
    //
    resolve(
      graphql(`
        {
          allDatoCmsPage(limit: 100) {
            edges {
              node {
                id
                slug
              }
            }
          }
        }
      `).then(result => {
        if (result.errors) {
          reject(result.errors)
        }
        result.data.allDatoCmsPage.edges.forEach(edge => {
          createPage({
            path: edge.node.slug,
            component: pageTemplate,
            context: {
              slug: edge.node.slug,
            },
          })
        })
        return
      })
    )
  })
}

// exports.createPages = ({ boundActionCreators, graphql }) => {
//   const { createPage } = boundActionCreators

//   return new Promise((resolve, reject) => {
//     graphql(`
//       {
//         allDatoCmsPage {
//           edges {
//             node {
//               slug
//             }
//           }
//         }
//       }
//     `).then(result => {
//       result.data.allDatoCmsPage.edges.map(({ node: page }) => {
//         createPage({
//           path: `pages/${page.slug}`,
//           component: path.resolve(`./src/templates/page.js`),
//           context: {
//             slug: page.slug,
//           },
//         });
//       });
//       resolve()
//     })
//   })
// }
